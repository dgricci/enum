package enum_test

import (
    "gitlab.com/dgricci/enum"
    "fmt"
)

// Examples :

func ExampleEnum_NewEnum() {
    colors := enum.NewEnum("Colors")
    if colors == nil {
        panic("Colors is not created")
    }
}

func ExampleEnum_Name() {
    colors := enum.NewEnum("Colors")
    fmt.Println(colors.Name())
    // Output:
    // Colors
}

func ExampleEnum_Add() {
    colors := enum.NewEnum("Colors")
    _, err := colors.Add("BLUE")
    if err != nil {
        panic("BLUE is not created")
    }
}

func ExampleEnum_Get() {
    colors := enum.NewEnum("Colors")
    colors.Add("BLUE")
    BLUE, _ := colors.Get("BLUE")
    if BLUE==nil {
        panic("BLUE is still not created")
    }
}

func ExampleEnum_Del() {
    colors := enum.NewEnum("Colors")
    colors.Add("BLUE")
    colors.Del("BLUE")
}

func ExampleEnum_String() {
    colors := enum.NewEnum("Colors")
    colors.Add("BLUE")
    colors.Add("WHITE")
    fmt.Println(colors.String())
    // Output:
    // Colors["BLUE","WHITE"]
}

