// Copyright 2017 Didier Richard IGN. All rights reserved.
// Use of this source code is governed by a Apache-style
// license that can be found in the LICENSE.md file.

/*
Package enum proposes an implementation for enumerations.

    var (
        colors  Enumeration     = enum.NewEnumeration("Colors", []string{"BLUE", "WHITE", "RED"})
        BLUE    EnumerationItem = colors.Get("BLUE")
        WHITE   EnumerationItem = colors.Get("WHITE")
        RED     EnumerationItem = colors.Get("RED")
    )
        
 */
package enum

import (
    "sync"
    "reflect"
    "fmt"
    "github.com/nicksnyder/go-i18n/i18n"
    "github.com/nicksnyder/go-i18n/i18n/language"
)

var (
    // tr function to translate. Use SetLang to assign.
    tr = i18n.IdentityTfunc()
    // lg current translation language. Use SetLang to assign.
    lg *language.Language
)

// SetLang function assigns a language for translations.
// Most language tags are a two character language code (ISO 639-1),
// optionally followed by a dash and a two character country code (ISO 3166-1)
// (e.g. en, fr-fr).
func SetLang (lang string) error {
    _t, _l, err := i18n.TfuncAndLanguage(lang)
    if err == nil {
        tr= _t
        lg= _l
    }
    return err
}

// enumDictionnary private type to hold enumeration sets.
//
type enumDictionnary struct {
    items       map[string]Enumeration  // map of enumeration sets
    m           sync.RWMutex
}

// setEnum function adds a new enumeration set.
// `enumName` is the enumeration set name.
//
func (e *enumDictionnary) setEnum(enumName string) Enumeration {
    e.m.Lock()
    defer e.m.Unlock()
    if _, exists := e.items[enumName] ; !exists {
        enum:= &enum{
            enumName: enumName,
            cardinal: 0,
            values  : make([]EnumerationItem, 0),
        }
        e.items[enumName]= enum
    }
    return e.items[enumName]
}

// enum function gets an existing enumeration set by its `enumName`.
//
func (e *enumDictionnary) enum(enumName string) (Enumeration, error) {
    e.m.RLock()
    defer e.m.RUnlock()
    enum, exists := e.items[enumName]
    if !exists {
        return nil, fmt.Errorf(tr("unknownEnum"), reflect.TypeOf(e).Name(), enumName)
    }
    return enum, nil
}

// del function removes an existing enumeration set by its `enumName`.
// The enumeration set should have been emptied before.
//
func (e *enumDictionnary) del(enumName string) {
    e.m.Lock()
    defer e.m.Unlock()
    if _, exists := e.items[enumName] ; exists {
        delete(e.items,enumName)
    }
    return
}

// private variables.
//
var (
    enums   *enumDictionnary    // It will be initialized once.
    once    sync.Once
)

// newEnumDictionnary function initialises the `enumDictionnary` variable if not already set.
// It is threads protected through `sync.Once`.
//
func newEnumDictionnary() *enumDictionnary {
    once.Do(func() {
        enums= &enumDictionnary{
            items: make(map[string]Enumeration),
        }
    })
    return enums
}

// enum type for an enumeration set.
// Threads protected when adding/reading an enumeration item.
//
type enum struct {
    enumName    string              // enumeration set name
    cardinal    int                 // count of all enumeration item ever created
    values      []EnumerationItem   // list of enumeration item
    m           sync.RWMutex
}

// Enumeration interface
type Enumeration interface {
    String()            string                      // representation of enumeration
    Name()              string                      // name of enumeration
    Values()            []EnumerationItem           // array of items fetching
    Add(string)         (EnumerationItem, error)    // new item addition
    Get(string)         (EnumerationItem, error)    // item retrieval
    Del(string)                                     // item removal
    NewEnumItem(string) EnumerationItem             // new item helper
}

// NewEnum function creates a new enumeration set by its `name`.
// If the enumeration already exists, it is returned.
// Returns `nil` whenever the creation fails.
//
func NewEnum(name string) Enumeration {
    enum, err := enums.enum(name)
    if err == nil {
        return enum
    }
    return enums.setEnum(name)
}

// String function returns a string representation of
// the Enumeration type.
//
func (e *enum) String() string {
    e.m.RLock()
    defer e.m.RUnlock()
    s := e.enumName + "["
    l := len(e.values)-1
    for i, ei := range e.values {
        s = s + ei.String()
        if i < l {
            s = s + ","
        }
    }
    s = s + "]"
    return s
}

// Name function returns the enumeration set's name.
//
func (e *enum) Name() string {
    return e.enumName
}

// Values function returns all the enumeration set's items.
//
func (e *enum) Values() []EnumerationItem {
    e.m.RLock()
    defer e.m.RUnlock()
    r := make([]EnumerationItem, 0)
    if len(e.values) > 0 {
        for i := range e.values {
            r= append(r, e.values[i])
        }
    }
    return r
}

// Add function adds an enumeration item to the enumeration set.
// `name` is the enumeration item human read value.
// Returns `nil` if the enumeration item already exists.
//
func (e *enum) Add(name string) (EnumerationItem, error) {
    e.m.Lock()
    defer e.m.Unlock()
    for _, i := range e.values {
        if i.Name() == name {
            return nil, fmt.Errorf(tr("alreadyKnownEnum"), i.EnumerationOf().Name(), name)
        }
    }
    e.cardinal++
    en := &enumItem{
            enum : e,
            name : name,
            ordinal : e.cardinal,
    }
    e.values= append(e.values, en)
    return en, nil
}

// Get function gets an enumeration item from an enumeration set by its `name`.
// Returns `nil` if the enumeration item does not exist.
//
func (e *enum) Get(name string) (EnumerationItem, error) {
    e.m.RLock()
    defer e.m.RUnlock()
    for _, i := range e.values {
        if i.Name() == name {
            return i, nil
        }
    }
    return nil, fmt.Errorf(tr("unknownEnum"), e.Name(), name)
}

// Del function removes an enumeration item from an enumeration set by its `name`.
// The ordinals are modified.
//
func (e *enum) Del(name string) {
    e.m.Lock()
    defer e.m.Unlock()
    r := -1
    for ri, ei := range e.values {
        if ei.Name() == name {
            r= ri
            /* as we don't update ordinal, we can break directly !
            continue*/
            break
        }
        /* no need, ordinal is set once !
        if r != -1 {// as a matter of fact, ri > r !
            // update ordinal for items after the to be removed one :
            ei.(*enumItem).ordinal--
        }*/
    }
    if r != -1 {//found, so cut the rth value
        // Cf. https://github.com/golang/go/wiki/SliceTricks
        e.values= append(e.values[:r], e.values[r+1:]...)
    }
}

// NewEnumItem function creates an enumeration item by its `name`.
// Returns `nil` when the enumeration creation fails.
//
func (e *enum) NewEnumItem(name string) EnumerationItem {
    ei, err := e.Add(name)
    if err != nil {
        return nil
    }
    return ei
}

// enumItem type for enumeration item.
//
type enumItem struct {
    enum        Enumeration         // enumeration set of this enumeration item
    name        string              // enumeration item name
    ordinal     int                 // enumeration item ordinal within its enumeration set
}

// EnumerationItem type
type EnumerationItem interface {
    String()            string      // representation of an item
    Name()              string      // value of item
    Ordinal()           int         // rank of item
    EnumerationOf()     Enumeration // enumeration the item belongs to
    Is(EnumerationItem) bool        // item comparison
}

// String function returns a representation of the
// enumeration item.
//
func (e *enumItem) String() string {
    return `"`+e.name+`"`
}

// Name function returns the enumeration item's name.
//
func (e *enumItem) Name() string {
    return e.name
}

// Ordinal function returns the enumeration item's ordinal.
//
func (e *enumItem) Ordinal() int {
    return e.ordinal
}

// EnumerationOf function returns the enumeration item's enumeration set.
//
func (e *enumItem) EnumerationOf() Enumeration {
    return e.enum
}

// Is function compares two enumeration items.
// There are equal when they belong to the same
// enumeration set and have the same ordinal.
//
func (e *enumItem) Is(n EnumerationItem) bool {
    return e.EnumerationOf() == n.EnumerationOf() && e.Ordinal() == n.Ordinal()
}

// NewEnumeration function is an helper function :
// it creates an enumeration set and its enumeration items.
// `enumName` is the enumeration set's name.
// `enumerations` is an array of enumeration item's name.
// Returns the enumeration set (`nil` whenever the creation fails).
//
func NewEnumeration(enumName string, enumerations []string) Enumeration {
    e := NewEnum(enumName)
    if e!= nil {
        for _, n := range enumerations {
            if e.NewEnumItem(n) == nil {
                // creation fails ...
                // FIXME: error handling : panic ?
                for _, ei := range e.Values() {
                    e.Del(ei.Name())
                    ei = nil
                }
                enums.del(enumName)
                return nil
            }
        }
    }
    return e
}

// init function initialises the enumeration sets dictionnary when importing this package.
//
func init() {
    i18n.MustLoadTranslationFile("i18n/en-us.all.json")
    i18n.MustLoadTranslationFile("i18n/fr-fr.all.json")

    newEnumDictionnary()
}

