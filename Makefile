# Copyright 2017-2018 Didier Richard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Adapted from https://github.com/vincentbernat/hellogopher/blob/master/Makefile
#
# To test without executing commands :
# $ make -n
# To debug :
# $ make --debug=[a|b|v|i|j|m]
#

# Print a * in blue :
#S        = $(shell printf "\033[34;1m*\033[0m")
# Print a * :
S        = $(shell printf "*")
# Print a TAB :
#T        = $(shell printf "\t")
# Print two spaces :
T        = $(shell printf "  ")
# Print a BACKSPACE :
#B        = $(shell printf "\b")

# top package
PKGROOT := gitlab.com/dgricci/enum
PKGNAME  =
PKGDIR   = ./$(PKGNAME)
# one can invoke with a sub-package by passing PACKAGE to the command line
PACKAGE ?= $(PKGROOT)
SUBPKGS  =

DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell [ -f $(PKGDIR)/version.go ] && cat $(PKGDIR)/version.go | grep "\w Version" | sed -e 's/[^"]*"\([^"]*\)"/\1/' || \
                git describe --tags --always --dirty --match=v* 2> /dev/null || \
                echo "0.0.0")
# one can override GOPATH by passing it to the command line. This is usefull
# when installing this package for production : make GOPATH=${GOPATH}
GOPATH   = $(CURDIR)/tmp-gopath
BIN      = $(GOPATH)/bin
BASE     = $(GOPATH)/src/$(PACKAGE)

GO       = go
GODOC    = godoc
DEP      = dep
GOLINT   = golint
GOI18N   = $(BIN)/goi18n

LDFLAGS  =-ldflags '-X $(PACKAGE).Version=$(VERSION) -X $(PACKAGE).BuildDate=$(DATE)'

export GOPATH

pkgPath=$(PKGROOT)
# remember : one can pass PACKAGE on the command-line like $ make PACKAGE=${PKGROOT}/subpkg
pkgName=$(shell basename $(PACKAGE))
ifneq ($(shell basename $(pkgPath)),$(pkgName))
pkgPfx=.$(PACKAGE:$(pkgPath)%=%)/
else
pkgPfx=./
endif
pkgFile=$(pkgPfx)$(pkgName)
coverOut=$(pkgFile)-coverprofile.out
coverHtml=$(pkgFile)-cover.html
pkgDoc=$(pkgFile)-doc.txt

# Default target (first target not starting with .)
all: main

packages:

ifeq ($(PACKAGE),$(PKGROOT))
# use make TRACE=1 to execute print-env target
# Default target (first target not starting with .)
main:: ; $(info $(S) Building the library …)
ifdef TRACE
main:: check-env print-env tools vendor packages tests install
else
main:: check-env tools vendor packages tests install
endif
main:: ; $(info … library built !)

# use make TRACE=1 to execute print-env target
ifdef TRACE
print-env:
	$(info $(T)PATH=$(PATH))
	$(info $(T)GOPATH=$(GOPATH))
	$(info $(T)pkgPath=$(pkgPath))
	$(info $(T)pkgName=$(pkgName))
	$(info $(T)pkgPfx=$(pkgPfx))
	$(info $(T)pkgFile=$(pkgFile))
	$(info $(T)coverOut=$(coverOut))
	$(info $(T)coverHtml=$(coverHtml))
	$(info $(T)pkgDoc=$(pkgDoc))
endif

# Source directory
$(BASE):: check-env
ifeq ("$(GOPATH)","$(CURDIR)/tmp-gopath")
	$(info $(T)$(S) setting temporary GOPATH …)
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR) $@
endif
$(BASE)::
ifeq ("$(GOPATH)","$(CURDIR)/tmp-gopath")
	$(info $(T)… set)
endif

# Check environment
# go is somewhere in the user's PATH :
check-env:: ; $(info $(T)$(T)$(S) checking for $(GO) …)
ifeq ($(shell command -v "$(GO)" >/dev/null 2>&1 || printf "0"),0)
check-env::
	$(error $(GO) is not in $$PATH.  Visit https://golang.org/dl/)
else
GOARCH  ?= $(shell uname -s | tr A-Z a-z)
GOOS    ?= $(subst x86_64,amd64,$(patsubst i%86,386,$(shell uname -m)))
check-env::
	$(info $(T)$(T)… found with GOARCH="$(GOARCH)" and GOOS="$(GOOS)")
endif

$(BIN):
	@mkdir -p $@
# the order-only pre-requisite $(BASE) is executed first if needed
$(BIN)/%:: $(BIN) | $(BASE) ; $(info $(T)$(T)$(S) installing $(@F) from $(REPOSITORY) …)
	@ tmp=$$(mktemp -d) ; \
		(env GOPATH=$$tmp $(GO) get -u $(REPOSITORY) && cp $$tmp/bin/* $(BIN)/.) || ret=$$?; \
		rm -rf $$tmp ; exit $$ret
$(BIN)/%:: ; $(info $(T)$(T) $(@F) installed !)

# Tools :
tools: tools-prolog dep golint goi18n tools-epilog
tools-prolog: $(BASE)
	$(info $(T)$(S) Checking/Installing tools …)
tools-epilog:
	$(info $(T)… done)

dep:: check-env ; $(info $(T)$(T)$(S) checking for $(DEP) …)
ifeq ($(shell command -v "$(DEP)" >/dev/null 2>&1 || printf "0"),0)
dep:: $(BIN)/dep
$(BIN)/dep: REPOSITORY=github.com/golang/dep/cmd/dep
else
dep::
	$(info $(T)$(T)… found)
endif

golint:: check-env ; $(info $(T)$(T)$(S) checking for $(GOLINT) …)
ifeq ($(shell command -v "$(GOLINT)" >/dev/null 2>&1 || printf "0"),0)
golint:: $(BIN)/golint
$(BIN)/golint: REPOSITORY=github.com/golang/lint/golint
else
golint::
	$(info $(T)$(T)… found)
endif

goi18n:: check-env ; $(info $(T)$(T)$(S) checking for $(GOI18N) …)
ifeq ($(shell command -v "$(GOI18N)" >/dev/null 2>&1 || printf "0"),0)
goi18n:: $(BIN)/goi18n
$(BIN)/goi18n: REPOSITORY=github.com/nicksnyder/go-i18n/goi18n
else
goi18n::
	$(info $(T)$(T)… found)
endif

ifeq ($(PACKAGE),$(PKGROOT))
# Dependencies through dep :
dep-init: $(BASE) ; $(info $(T)$(T)$(S) creating dependencies from scratch …)
	@cd $(BASE) && ( [ ! -f Gopkg.toml ] && $(DEP) init || touch Gopkg.toml )
dep-install: Gopkg.toml ; $(info $(T)$(T)$(S) retrieving dependencies …)
	@$(DEP) ensure

# protect lf -> \n\
# protect "  -> \"
# protect $  -> $$
# protect $0 from being replaced by /bin/sh -> \$$0
# protect \n -> \\\n
define AWKSCRIPT
#!/usr/bin/awk -f\n\
BEGIN{\n\
  P=0\n\
  U=\"\"\n\
}\n\
/^#/{\n\
  next\n\
}\n\
/^$$/{\n\
  next\n\
}\n\
/^\[\[projects\]\]/{\n\
  P=1\n\
  next\n\
}\n\
{\n\
  if (P==1 && match(\$$0,/\s*name\s*=\s*\"([^\"]*)\"/,v)>0) {\n\
    U=v[1]\n\
    next\n\
  }\n\
  if (P==1 && U!=\"\" && match(\$$0,/\s*packages\s*=\s*\[([^]]*)\]/,p)>0) {\n\
    n = split(p[1],pks,/,/)\n\
    if (n>0) {\n\
      pkg=pks[1]\n\
      gsub(/\"/,\"\",pkg)\n\
      if (pkg == \".\") {\n\
        printf \"%s\\\n\", U\n\
      } else {\n\
        printf \"%s/%s\\\n\", U, pkg\n\
      }\n\
      P=0\n\
      U=\"\"\n\
      next\n\
    }\n\
  }\n\
}\n\
END{}\n
endef

Gopkg.awk: Gopkg.toml Gopkg.lock | $(BASE) dep
	@echo "$(AWKSCRIPT)" > $@

vendor: Gopkg.toml Gopkg.lock Gopkg.awk | $(BASE) dep ; $(info $(T)$(T)$(S) installing dependencies …)
	@cd $(BASE) && $(DEP) ensure -update
	@for p in `awk -f Gopkg.awk Gopkg.lock` ; do cd vendor/$$p && $(GO) install . && cd - >/dev/null ; done
	@rm -f Gopkg.awk
endif

# Package tests
tests: tests-prolog clean tools vendor build-tests tests-epilog

tests-prolog: $(BASE)
	$(info $(S) Building environment and testing …)

tests-epilog:
	$(info … tested)

clean: clean-prolog clean-tmp clean-i18n clean-package clean-epilog

clean-prolog: $(BASE)
	$(info $(T)$(S) Cleaning …)

clean-tmp:
	@rm -f "$(coverOut)" "$(coverHtml)" "$(pkgDoc)"

clean-i18n:
	@rm -f $(pkgPfx)i18n/*.all.json $(pkgPfx)i18n/*.untranslated.json

clean-package:
	@rm -f Gopkg.awk
	@$(GO) clean $(PACKAGE)

clean-epilog:
ifeq ("$(GOPATH)","$(CURDIR)/tmp-gopath")
	@rm -rf $(GOPATH)
endif
	$(info $(T)… cleaned)

build-tests: build-i18n test-lint doc

build-i18n: build-i18n-prolog goi18n $(pkgPfx)i18n/en-us.all.json $(pkgPfx)i18n/fr-fr.all.json build-i18n-epilog

build-i18n-prolog: $(BASE)
	$(info $(T)$(S) Generating i18n files …)

build-i18n-epilog:
	$(info $(T)… generation done)

%.all.json: %.toml
	$(eval lang=$(shell basename `echo $*`))
	$(eval i18n=$(shell dirname `echo $@`))
	@$(GOI18N) -sourceLanguage $(lang) -outdir $(i18n) ./$(i18n)/$(lang).toml

test-lint: test-lint-prolog count cover test-lint-epilog

test-lint-prolog:
	$(info $(T)$(S) Preparing, Calculating coverage and Linting …)
count:
	@$(GO) test $(GOTESTOPTS) \
		$(LDFLAGS) \
		-covermode=count -coverprofile="$(coverOut)" $(PACKAGE)

cover:
	@$(GO) tool cover -func="$(coverOut)"
	@$(GO) tool cover -html="$(coverOut)" -o "$(coverHtml)"
	@rm -f "$(coverOut)"
	@echo firefox "$(coverHtml)" to see $(PACKAGE) code\'s coverage \(heat map\)
	@$(GOLINT) $(PACKAGE)
	@$(GO) vet $(PACKAGE)

test-lint-epilog:
	$(info $(T)$(S) … tests, coverage and lint done)

doc: doc-prolog doc-godoc doc-epilog

doc-prolog: ; $(info $(T)$(S) Generating text document in $(pkgDoc) …)
	@$(GODOC) $(PACKAGE) | sed '1d' > "$(pkgDoc)"

doc-godoc: ; $(info $(T)$(S) One can also launch $$ godoc -http=:unused-port)
	$(info $(T)$(T)say unused-port is 6060 then)
	$(info $(T)$(T)point your web browser at 'http://localhost:6060/pkg')
	$(info $(T)$(T)the list of standard and local packages are displayed.)

doc-epilog:
	$(info $(T)… documentation done)

# Installation of the package
install: install-prolog clean build-i18n install-package doc install-epilog

install-prolog: $(BASE)
	$(info $(S) Installing $(PACKAGE) …)

install-package:
	@env GOARCH=$(GOARCH) GOOS=$(GOOS) $(GO) install \
		$(LDFLAGS) $(PACKAGE)

install-epilog:
	$(info … installation done)

help:
	$(info $(S) In order to build and install this package $(PACKAGE), you need to have golang installed.)
	$(info $(S) One must run the make command from the $(PKGROOT) directory.)
	$(info $(S) One can use its own GOPATH environment for buidling this package :)
	$(info $(T)$$ make GOPATH=$${GOPATH})
	$(info $(T)By default, this makefile uses is own GOPATH, current value is :)
	$(info $(T)$$ GOPATH=$(GOPATH))
	$(info $(S) To run the tests, use :)
	$(info $(T)$$ make [GOPATH=$(GOPATH)] tests)
	$(info $(T)One can pass GOTESTOPTS in the form GOTESTOPTS="options" as in:)
	$(info $(T)$$ make [GOPATH=$(GOPATH)] GOTESTOPTS="-v" tests)
	$(info $(S) To install the package, use :)
	$(info $(T)$$ make [GOPATH=$(GOPATH)] install)
	$(info $(T)One can pass GOARCH and GOOS for cross-compiling :)
	$(info $(T)$$ GOARM=7 make [GOPATH=$(GOPATH)] GOARCH=arm GOOS=linux install)
	$(info Default target both runs the tests and installs the package)
	@$(MAKE) --silent check-env

# always execute these targets when invoked
.PHONY: all tools tests clean build-tests doc install help

