% Enum : Énumérations comme en Java ... ou presque !  
% Didier Richard  
% 04/01/2018

---

Révision : 
* 11/04/2017 : initialisation du projet
* 09/09/2017 : premier ajout du Makefile
* 02/11/2017 : utilisation de dep
* 04/01/2018 : Makefile testé

---

# Objectif #

Apprendre le langage golang en mettant en œuvre un concept simple : les
énumérations à l'instar de $Java^{TM}$ !

# Qu'est-ce qu'un énuméré ? #

Une énumération est un type de données pour lequel une variable ne peut
prendre qu'un nombre fini de valeurs qui sont des constantes nommées.

Une énumération est donc un type de données qui expose une API permettant
d'accéder à ce nombre fini de valeurs.

Les valeurs sont aussi un type de données qui expose une API permettant
d'obtenir leur représentation, leur énumération d'origine, de les comparer,
etc ...

# Installation #

## Dépendences ##

Il est nécessaire d'avoir installé [go](https://golang.org/dl/) :-)
L'installation de [dep](https://github.com/golang/dep), de
[golint](https://github.com/golang/lint) et de
[goi18n](https://github.com/nicksnyder/go-i18n) sera effectué par le Makefile
si besoin.

## Compilation ##

Pour compiler cette bibliothèque, il suffit de lancer :

```bash
$ cd $GOPATH/src
$ git clone https://gitlab.com/dgricci/enum.git ./gitlab.com/dgricci/enum
$ cd ./gitlab.com/dgricci/enum && make GOPATH=${GOPATH}
```

