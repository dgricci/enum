package enum_test

import (
    "gitlab.com/dgricci/enum"
    "fmt"
)

// Examples :

var (
    colors enum.Enumeration = enum.NewEnum("Colors")
)

func ExampleEnumItem_NewEnumItem() {
    BLUE, _ := colors.Get("BLUE")
    if BLUE == nil {
        BLUE = colors.NewEnumItem("BLUE")
    }
    if BLUE == nil {
        panic("BLUE not created")
    }
}

func ExampleEnumItem_Name() {
    BLUE, _ := colors.Get("BLUE")
    if BLUE == nil {
        BLUE = colors.NewEnumItem("BLUE")
    }
    if BLUE == nil {
        panic("BLUE not created")
    }
    fmt.Println(BLUE.Name())
    // Output:
    // BLUE
}

func ExampleEnumItem_Ordinal() {
    BLUE, _ := colors.Get("BLUE")
    if BLUE == nil {
        BLUE = colors.NewEnumItem("BLUE")
    }
    if BLUE == nil {
        panic("BLUE not created")
    }
    fmt.Println(BLUE.Ordinal())
    // Output:
    // 1
}

func ExampleEnumItem_String() {
    BLUE, _ := colors.Get("BLUE")
    if BLUE == nil {
        BLUE = colors.NewEnumItem("BLUE")
    }
    fmt.Println(BLUE.String())
    // Output:
    // "BLUE"
}

