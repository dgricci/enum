package enum

import (
    "testing"
)

// Tests :

func TestColorEnum (t *testing.T) {
    n := "Color"
    e := NewEnum(n)
    if e == nil {
        t.Errorf("Enum '%s' not created ?", n)
    }
    if NewEnum(n) == nil {
        t.Errorf("Enum '%s' not yet created ?", n)
    }
    e2, err := enums.enum(n)
    if err != nil {
        t.Errorf("Enum '%s' not found cause of '%s'.", n, err)
    }
    if e != e2 {
        t.Errorf("Enum '%s' is not unique : %+v != %+v.", n, e, e2)
    }
    if len(e.Values()) != 0 {
        t.Errorf("Enum '%s' has EnumItem : %+v.", n, e.Values())
    }
    if e.Name() != n {
        t.Errorf("Enum '%s' has got a different name : '%s'.", n, e.Name())
    }
    BLUE := EnumerationItem(nil)
    b := "BLUE"
    BLUE, err = e.Add(b)
    if BLUE == nil {
        t.Errorf("EnumItem '%s' not created cause of '%s'.", b, err)
    }
    if len(e.Values()) != 1 {
        t.Errorf("Enum '%s' has '%d' EnumItem, 1 expected : %+v.", n, len(e.Values()), e.Values())
    }
    if _, err = e.Add(b) ; err == nil {
        t.Errorf("EnumItem '%s' should not be created twice.", b)
    }
    BLUE2 := e.NewEnumItem(b)
    if BLUE2 != nil {
        t.Errorf("EnumItem '%s' should not be created twice again.", b)
    }
    if BLUE2, _= e.Get(b) ; !BLUE.Is(BLUE2) {
        t.Errorf("EnumItem '%s' and '%s' should have been equal.", BLUE2.Name(), BLUE.Name())
    }
    WHITE := EnumerationItem(nil)
    b = "WHITE"
    WHITE, err = e.Add(b)
    if WHITE == nil {
        t.Errorf("EnumItem '%s' not created cause of '%s'.", b, err)
    }
    if len(e.Values()) != 2 {
        t.Errorf("Enum '%s' has '%d' EnumItem, 1 expected : %+v.", n, len(e.Values()), e.Values())
    }
    if WHITE.Is(BLUE) {
        t.Errorf("'%s' should not be equal to '%s'.", BLUE.Name(), WHITE.Name())
    }
    n= "Color2"
    e2= NewEnum(n)
    if e == nil {
        t.Errorf("Enum '%s' not created ?", n)
    }
    WHITE2 := e2.NewEnumItem(b)
    if WHITE2 == nil {
        t.Errorf("Enum '%s' EnumItem '%s' not created.", e2.Name(), b)
    }
    if WHITE2.EnumerationOf() != e2 {
        t.Errorf("EnumItem '%s' does not belongs to expected Enum '%s'.", WHITE2.Name(), e2.Name())
    }
    if WHITE.Is(WHITE2) {
        t.Errorf("(%s)'%s' should not be equal to (%s)'%s'.", e.Name(), BLUE.Name(), e2.Name(), WHITE.Name())
    }
    WHITE2, err = e.Get(b)
    if !WHITE.Is(WHITE2) {
        t.Errorf("(%s)'%s' should be equal to (%s)'%s'.", e.Name(), WHITE.Name(), e.Name(), WHITE2.Name())
    }
    b= "RED"
    RED := e.NewEnumItem(b)
    if RED == nil {
        t.Errorf("EnumItem '%s' not created.", b)
    }
    if len(e.Values()) != 3 {
        t.Errorf("Enum '%s' has '%d' EnumItem, 1 expected : %+v.", e.Name(), len(e.Values()), e.Values())
    }
    if RED.Ordinal() != len(e.Values()) {
        t.Errorf("EnumItem '%s' has '%d' as ordinal, expected '%d'.", RED.Name(), RED.Ordinal(), len(e.Values()))
    }
    enums.del(n)
    _, err = enums.enum(n)
    if err == nil {
        t.Errorf("Enum '%s' should have been deleted ...", n)
    }
    b= "WHITE"
    e.Del(b)
    if _, err = e.Get(b) ; err == nil {
            t.Errorf("EnumItem '%s' should have been deleted ...", b)
    }
    if len(e.Values()) != 2 {
        t.Errorf("Enum '%s' should have only two items, '%d' found : %v+.", e.Name(), len(e.Values()), e.Values())
    }
    nb := len(enums.items)
    n= "Color"
    enums.del(n)
    _, err = enums.enum(n)
    if err == nil {
        t.Errorf("Enum '%s' should have been deleted ...", n)
    }
    if len(enums.items) != nb-1 {
        t.Errorf("There should be %d Enum : %d found : %+v.", nb-1, len(enums.items), enums.items)
    }
}

func TestNewEnumerationFailure (t *testing.T) {
    nilENUM := NewEnumeration("GO", []string{"G", "O", "L", "A", "N", "G"})
    if (nilENUM != nil) {
        t.Errorf("Oops ... should have failed !")
    }
}

func TestRainbow (t *testing.T) {
    RAINBOW := NewEnumeration("RAINBOW", []string{"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET"})
    if RAINBOW == nil {
        t.Errorf("No RAINBOW ...")
    }
    RED, _ := RAINBOW.Get("RED")
    if RED == nil {
        t.Errorf("No Red color in RAINBOW : %+v.", RAINBOW)
    }
    ORANGE, _ := RAINBOW.Get("ORANGE")
    if ORANGE == nil {
        t.Errorf("No Orange color in RAINBOW : %+v.", RAINBOW)
    }
    YELLOW, _ := RAINBOW.Get("YELLOW")
    if YELLOW == nil {
        t.Errorf("No Yellow color in RAINBOW : %+v.", RAINBOW)
    }
    GREEN, _ := RAINBOW.Get("GREEN")
    if GREEN == nil {
        t.Errorf("No Green color in RAINBOW : %+v.", RAINBOW)
    }
    BLUE, _ := RAINBOW.Get("BLUE")
    if BLUE == nil {
        t.Errorf("No Blue color in RAINBOW : %+v.", RAINBOW)
    }
    INDIGO, _ := RAINBOW.Get("INDIGO")
    if INDIGO == nil {
        t.Errorf("No Indigo color in RAINBOW : %+v.", RAINBOW)
    }
    VIOLET, _ := RAINBOW.Get("VIOLET")
    if VIOLET == nil {
        t.Errorf("No Violet color in RAINBOW : %+v.", RAINBOW)
    }
}

func TestSetLang ( t* testing.T ) {
    const id = "unknownEnum"
    if tr(id) != id {
        t.Errorf("No translation function set, but translation active ?!")
    }
    err := SetLang("en")
    if err != nil {
        t.Errorf(err.Error())
    }
    var en= tr(id)
    if en == id {
        t.Errorf("'%s' not translated in %s ?!", id, lg.String())
    }
    err = SetLang("it")
    if err == nil {
        t.Errorf("it supported ?!")
    }
    err = SetLang("fr")
    if err != nil {
        t.Errorf("fr not supported !?")
    }
    var fr= tr(id)
    if fr == id {
        t.Errorf("'%s' not translated in %s ?!", id, lg.String())
    }
    if en == fr {
        t.Errorf("'%s' not translated at all ?!", id)
    }
}

